package com.sensorcon.LogDatabase;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class SensorLogDataSource {

    private SQLiteDatabase db;
    private SensorLogDatabaseHelper dbHelper;
    private String[] allSensorLogColumns = { SensorLogDatabaseHelper.COLUMN_LOGS_ID,
            SensorLogDatabaseHelper.COLUMN_LOGS_TIMESTAMP,
            SensorLogDatabaseHelper.COLUMN_LOGS_MAC,
            SensorLogDatabaseHelper.COLUMN_LOGS_TEMP,
            SensorLogDatabaseHelper.COLUMN_LOGS_HUMIDITY,
            SensorLogDatabaseHelper.COLUMN_LOGS_PRESSURE,
            SensorLogDatabaseHelper.COLUMN_LOGS_CO,
            SensorLogDatabaseHelper.COLUMN_LOGS_LIGHT};

    private String[] allDeviceColumns = { SensorLogDatabaseHelper.COLUMN_DEVICES_ID,
            SensorLogDatabaseHelper.COLUMN_DEVICES_MAC,
            SensorLogDatabaseHelper.COLUMN_DEVICES_NAME,
            SensorLogDatabaseHelper.COLUMN_DEVICES_LOC};

    public SensorLogDataSource(Context context) {
        dbHelper = new SensorLogDatabaseHelper(context);
    }

    public void open() throws SQLException {
        db = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public SensorLog createLog(String timestamp, String mac, float temp, float humidity, float pressure, float co, float light) {

        ContentValues values = new ContentValues();
        values.put(SensorLogDatabaseHelper.COLUMN_LOGS_TIMESTAMP, timestamp);
        values.put(SensorLogDatabaseHelper.COLUMN_LOGS_MAC, mac);
        values.put(SensorLogDatabaseHelper.COLUMN_LOGS_TEMP, temp);
        values.put(SensorLogDatabaseHelper.COLUMN_LOGS_HUMIDITY, humidity);
        values.put(SensorLogDatabaseHelper.COLUMN_LOGS_PRESSURE, pressure);
        values.put(SensorLogDatabaseHelper.COLUMN_LOGS_CO, co);
        values.put(SensorLogDatabaseHelper.COLUMN_LOGS_LIGHT, light);

        long insertId = db.insert(SensorLogDatabaseHelper.TABLE_LOGS, null, values);
        Cursor cursor = db.query(SensorLogDatabaseHelper.TABLE_LOGS, allSensorLogColumns, SensorLogDatabaseHelper.COLUMN_LOGS_ID + " = " + insertId, null, null, null, null);
        cursor.moveToFirst();
        SensorLog log = cursorToLog(cursor);
        cursor.close();

        return log;
    }

    public void deleteLog(SensorLog log) {
        long id = log.getId();
        db.delete(SensorLogDatabaseHelper.TABLE_LOGS, SensorLogDatabaseHelper.COLUMN_LOGS_ID + " = " + id, null);
    }

    public void deleteLog(String timestamp, String MAC) {
        db.delete(SensorLogDatabaseHelper.TABLE_LOGS,
                SensorLogDatabaseHelper.COLUMN_LOGS_TIMESTAMP + " = " + "\"" + timestamp +  "\"" + " AND " + SensorLogDatabaseHelper.COLUMN_LOGS_MAC + " = " + "\"" + MAC + "\"",
                null);
    }

    public List<SensorLog> getAllLogs() {

        List<SensorLog> logs = new ArrayList<SensorLog>();

        Cursor cursor = db.query(SensorLogDatabaseHelper.TABLE_LOGS, allSensorLogColumns, null, null, null, null, null);
        cursor.moveToFirst();

        while(!cursor.isAfterLast()) {
            SensorLog log = cursorToLog(cursor);
            logs.add(log);
            cursor.moveToNext();
        }

        cursor.close();
        return logs;
    }

    public List<SensorLog> getLogsFromMAC(String mac) {

        List<SensorLog> logs = new ArrayList<SensorLog>();

        Cursor cursor = db.query(SensorLogDatabaseHelper.TABLE_LOGS, allSensorLogColumns, SensorLogDatabaseHelper.COLUMN_LOGS_MAC + "=?", new String[] {mac}, null, null, null);
        cursor.moveToFirst();

        while(!cursor.isAfterLast()) {
            SensorLog log = cursorToLog(cursor);
            logs.add(log);
            cursor.moveToNext();
        }

        cursor.close();
        return logs;
    }

    private SensorLog cursorToLog(Cursor cursor) {

        SensorLog log = new SensorLog();
        log.setId(cursor.getLong(0));
        log.setTimestamp((cursor.getString(1)));
        log.setMACAddress((cursor.getString(2)));
        log.setTemperature((cursor.getFloat(3)));
        log.setHumidity((cursor.getFloat(4)));
        log.setPressure((cursor.getFloat(5)));
        log.setCO((cursor.getFloat(6)));
        log.setLight((cursor.getFloat(7)));

        return log;
    }

    public Device createDevice(String mac, String name, String location) {

        ContentValues values = new ContentValues();
        values.put(SensorLogDatabaseHelper.COLUMN_DEVICES_MAC, mac);
        values.put(SensorLogDatabaseHelper.COLUMN_DEVICES_NAME, name);
        values.put(SensorLogDatabaseHelper.COLUMN_DEVICES_LOC, location);

        long insertId = db.insert(SensorLogDatabaseHelper.TABLE_DEVICES, null, values);
        Cursor cursor = db.query(SensorLogDatabaseHelper.TABLE_DEVICES, allDeviceColumns, SensorLogDatabaseHelper.COLUMN_DEVICES_ID + " = " + insertId, null, null, null, null);
        cursor.moveToFirst();
        Device device = cursorToDevice(cursor);
        cursor.close();

        return device;
    }

    public void deleteDevice(Device device) {
        long id = device.getId();
        db.delete(SensorLogDatabaseHelper.TABLE_DEVICES, SensorLogDatabaseHelper.COLUMN_DEVICES_ID + " = " + id, null);
    }

    public List<Device> getAllDevices() {

        List<Device> devices = new ArrayList<Device>();

        Cursor cursor = db.query(SensorLogDatabaseHelper.TABLE_DEVICES, allDeviceColumns, null, null, null, null, null);
        cursor.moveToFirst();

        while(!cursor.isAfterLast()) {
            Device device = cursorToDevice(cursor);
            devices.add(device);
            cursor.moveToNext();
        }

        cursor.close();
        return devices;
    }

    private Device cursorToDevice(Cursor cursor) {

        Device device = new Device();
        device.setId(cursor.getLong(0));
        device.setMACAddress((cursor.getString(1)));
        device.setName((cursor.getString(2)));
        device.setLocation((cursor.getString(3)));

        return device;
    }

    public void deleteAll() {
        db.execSQL("delete from " + SensorLogDatabaseHelper.TABLE_LOGS);
        db.execSQL("delete from " + SensorLogDatabaseHelper.TABLE_DEVICES);
    }
}
