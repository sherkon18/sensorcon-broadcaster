package com.sensorcon.LogDatabase;


public class SensorLog {

    private long mId;
    private String mTimestamp;
    private String mMACAddress;
    private float mTemperature;
    private float mHumidity;
    private float mPressure;
    private float mLight;
    private float mCO;

    public long getId() {
        return mId;
    }

    public void setId(long id) {
        mId = id;
    }

    public String getTimestamp() {
        return mTimestamp;
    }

    public void setTimestamp(String timestamp) {
        mTimestamp = timestamp;
    }

    public String getMACAddress() {
        return mMACAddress;
    }

    public void setMACAddress(String address) {
        mMACAddress = address;
    }

    public float getTemperature() {
        return mTemperature;
    }

    public void setTemperature(float temperature) {
        mTemperature = temperature;
    }

    public float getHumidity() {
        return mHumidity;
    }

    public void setHumidity(float humidity) {
        mHumidity = humidity;
    }

    public float getPressure() {
        return mPressure;
    }

    public void setPressure(float pressure) {
        mPressure = pressure;
    }

    public float getLight() {
        return mLight;
    }

    public void setLight(float light) {
        mLight = light;
    }

    public float getCO() {
        return mCO;
    }

    public void setCO(float co) {
        mCO = co;
    }

    public String toString() {
        String st = "[Timestamp " + mTimestamp + "]";
        st += " [MAC " + mMACAddress + "]";
        st += " [Temperature " + mTemperature + "]";
        st += " [Humidity " + mHumidity + "]";
        st += " [Pressure " + mPressure + "]";
        st += " [Light " + mLight + "]";
        st += " [CO " + mCO + "]";

        return st;
    }
}
